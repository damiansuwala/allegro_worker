﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allegro_worker
{
    class AllegroItem
    {
        private string aItemID;
        private string aItemName;
        private string aItemQty;

        public AllegroItem(string aItemID, string aItemName, string aItemQty)
        {
            this.aItemID = aItemID;
            this.aItemName = aItemName;
            this.aItemQty = aItemQty;
        }

        public string AItemID { get => aItemID; set => aItemID = value; }
        public string AItemName { get => aItemName; set => aItemName = value; }
        public string AItemQty { get => aItemQty; set => aItemQty = value; }



    }
}
