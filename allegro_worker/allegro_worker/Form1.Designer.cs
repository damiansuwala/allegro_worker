﻿namespace allegro_worker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numShoperAuctions = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.numAllegroAuctions = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_checkCohesion = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lbx_log = new System.Windows.Forms.ListBox();
            this.ustawienia = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TXT_alle_pswd = new System.Windows.Forms.TextBox();
            this.TXT_alle_username = new System.Windows.Forms.TextBox();
            this.TXT_alle_WebApi = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.ustawienia.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.ustawienia);
            this.tabControl1.Location = new System.Drawing.Point(5, 15);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(792, 638);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.numShoperAuctions);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.numAllegroAuctions);
            this.tabPage1.Controls.Add(this.listBox1);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.btn_checkCohesion);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.lbx_log);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(784, 609);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Sprawdzanie spójności";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 247);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(281, 17);
            this.label11.TabIndex = 9;
            this.label11.Text = "Aukcje, w których występuje różnica stanów";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(528, 55);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(154, 17);
            this.label10.TabIndex = 8;
            this.label10.Text = "Liczba aukcji w sklepie:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numShoperAuctions
            // 
            this.numShoperAuctions.Enabled = false;
            this.numShoperAuctions.Location = new System.Drawing.Point(705, 52);
            this.numShoperAuctions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numShoperAuctions.Name = "numShoperAuctions";
            this.numShoperAuctions.Size = new System.Drawing.Size(61, 22);
            this.numShoperAuctions.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(528, 17);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Liczba aukcji na allegro:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // numAllegroAuctions
            // 
            this.numAllegroAuctions.Enabled = false;
            this.numAllegroAuctions.Location = new System.Drawing.Point(705, 14);
            this.numAllegroAuctions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numAllegroAuctions.Name = "numAllegroAuctions";
            this.numAllegroAuctions.Size = new System.Drawing.Size(61, 22);
            this.numAllegroAuctions.TabIndex = 5;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(9, 87);
            this.listBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(759, 100);
            this.listBox1.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 64);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(197, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Aukcje, których brak w sklepie";
            // 
            // btn_checkCohesion
            // 
            this.btn_checkCohesion.Location = new System.Drawing.Point(8, 7);
            this.btn_checkCohesion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_checkCohesion.Name = "btn_checkCohesion";
            this.btn_checkCohesion.Size = new System.Drawing.Size(169, 28);
            this.btn_checkCohesion.TabIndex = 2;
            this.btn_checkCohesion.Text = "Sprawdź spójność";
            this.btn_checkCohesion.UseVisualStyleBackColor = true;
            this.btn_checkCohesion.Click += new System.EventHandler(this.btn_checkCohesion_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 490);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Log";
            // 
            // lbx_log
            // 
            this.lbx_log.BackColor = System.Drawing.SystemColors.Info;
            this.lbx_log.FormattingEnabled = true;
            this.lbx_log.ItemHeight = 16;
            this.lbx_log.Location = new System.Drawing.Point(8, 513);
            this.lbx_log.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbx_log.Name = "lbx_log";
            this.lbx_log.Size = new System.Drawing.Size(764, 84);
            this.lbx_log.TabIndex = 0;
            // 
            // ustawienia
            // 
            this.ustawienia.BackColor = System.Drawing.Color.Transparent;
            this.ustawienia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ustawienia.Controls.Add(this.groupBox2);
            this.ustawienia.Controls.Add(this.groupBox1);
            this.ustawienia.Location = new System.Drawing.Point(4, 25);
            this.ustawienia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ustawienia.Name = "ustawienia";
            this.ustawienia.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ustawienia.Size = new System.Drawing.Size(784, 609);
            this.ustawienia.TabIndex = 1;
            this.ustawienia.Text = "ustawienia";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.textBox5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.No;
            this.groupBox2.Location = new System.Drawing.Point(383, 7);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(369, 180);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Shoper";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(117, 121);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox4.Name = "textBox4";
            this.textBox4.PasswordChar = '*';
            this.textBox4.Size = new System.Drawing.Size(228, 22);
            this.textBox4.TabIndex = 11;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(117, 79);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(228, 22);
            this.textBox5.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 43);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Adres sklepu";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(117, 39);
            this.textBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(228, 22);
            this.textBox6.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 82);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Login";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 126);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Hasło";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TXT_alle_pswd);
            this.groupBox1.Controls.Add(this.TXT_alle_username);
            this.groupBox1.Controls.Add(this.TXT_alle_WebApi);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(367, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Allegro";
            // 
            // TXT_alle_pswd
            // 
            this.TXT_alle_pswd.Location = new System.Drawing.Point(91, 117);
            this.TXT_alle_pswd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TXT_alle_pswd.Name = "TXT_alle_pswd";
            this.TXT_alle_pswd.PasswordChar = '*';
            this.TXT_alle_pswd.Size = new System.Drawing.Size(244, 22);
            this.TXT_alle_pswd.TabIndex = 5;
            // 
            // TXT_alle_username
            // 
            this.TXT_alle_username.Location = new System.Drawing.Point(91, 79);
            this.TXT_alle_username.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TXT_alle_username.Name = "TXT_alle_username";
            this.TXT_alle_username.Size = new System.Drawing.Size(244, 22);
            this.TXT_alle_username.TabIndex = 4;
            // 
            // TXT_alle_WebApi
            // 
            this.TXT_alle_WebApi.Location = new System.Drawing.Point(91, 39);
            this.TXT_alle_WebApi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TXT_alle_WebApi.Name = "TXT_alle_WebApi";
            this.TXT_alle_WebApi.Size = new System.Drawing.Size(244, 22);
            this.TXT_alle_WebApi.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 121);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Hasło";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 82);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Login";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "WebAPI";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 667);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ustawienia.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage ustawienia;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TXT_alle_pswd;
        private System.Windows.Forms.TextBox TXT_alle_username;
        private System.Windows.Forms.TextBox TXT_alle_WebApi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lbx_log;
        private System.Windows.Forms.Button btn_checkCohesion;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox numShoperAuctions;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox numAllegroAuctions;
    }
}

