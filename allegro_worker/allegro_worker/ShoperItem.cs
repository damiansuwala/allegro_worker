﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allegro_worker
{
    class ShoperItem
    {
        public string sItemID { get; set; }
        public string sItemName { get; set; }
        public string sItemQty { get; set; }

        public ShoperItem(string sItemID, string sItemName, string sItemQty)
        {
            this.sItemID = sItemID;
            this.sItemName = sItemName;
            this.sItemQty = sItemQty;
        }



    }
}
