﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using allegro_worker.allegro.webapi;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;

namespace allegro_worker
{
    public partial class Form1 : Form
    {
        // VARIABLES FOR ALLEGRO SERVICE
        const int countryCode = 1;
        string webapiKey = "b54e1740";
        long verKey = 0;

        //VAR FOR SHOPER
        string countShoperItems = "";
        RestClient client = new RestClient("https://wezeizlacza.pl");
        List<ShoperItem> shoperAuctions = new List<ShoperItem>();

        public class ShoperAuction
        {
            public string auction_id { get; set; }
            public string real_auction_id { get; set; }
            public string auction_house_id { get; set; }
            public string sales_format { get; set; }
            public string title { get; set; }
            public string product_id { get; set; }
            public string stock_id { get; set; }
            public string quantity { get; set; }
            public string sold { get; set; }
            public string start_time { get; set; }
            public object end_time { get; set; }
            public string start_price { get; set; }
            public string buy_now_price { get; set; }
            public string min_price { get; set; }
            public object best_price { get; set; }
            public string views { get; set; }
            public string binds { get; set; }
            public string cost { get; set; }
            public string status_time { get; set; }
            public string finished { get; set; }
        }

        public class RootObject
        {
            public string count { get; set; }
            public int pages { get; set; }
            public int page { get; set; }
            public List<ShoperAuction> list { get; set; }
        }

        public Form1()
        {
            InitializeComponent();
            lbx_log.Items.Add("Witaj!");
        }


        public long GetVerCode(string webapikey)
        {
            AllegroWebApiService api = new AllegroWebApiService();
            try
            {
                // get data
                SysStatusType[] ssta = api.doQueryAllSysStatus(countryCode, webapikey);
                foreach (SysStatusType sst in ssta)
                {
                    lbx_log.Items.Add("ALLEGRO: Pomyślnie pobrano klucz wersji: " + sst.verkey);
                    return sst.verkey;
                }
            }
            catch (System.Web.Services.Protocols.SoapException ee)
            {
                // ops... error!?...
                lbx_log.Items.Add("Error: Code=" + ee.Code.Name + Environment.NewLine + ee.Message);
                return 0;
            }

            return 0;
        }

        private void btn_checkCohesion_Click(object sender, EventArgs e)
        {
            if(verKey == 0) {
                btn_checkCohesion.Enabled = false;
                verKey = GetVerCode(webapiKey);
                btn_checkCohesion.Enabled = true;
            }

            countShoperItems = getShoperCountAuction();
            numShoperAuctions.Text = countShoperItems;
            Console.WriteLine("countShoperItems: " + countShoperItems);

            string auth_token = getShoperAuthToken();
            var rootObject = JsonConvert.DeserializeObject<RootObject>(getShoperListAuctions(auth_token));

            foreach (var i in rootObject.list)
            {
                shoperAuctions.Add(new ShoperItem(i.real_auction_id, i.title, i.quantity));
                lbx_log.Items.Add("Id: "+i.real_auction_id + ": "+i.title+" - "+i.quantity+" szt.");
            }
        
        }

        private string getShoperListAuctions(string auth_token)
        {
            string json_response = "";
            //get list of auctions from shoper REST API
            var request = new RestRequest("/webapi/rest/auctions", Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + auth_token), ParameterType.HttpHeader);
            request.AddParameter("offset", "0");

            IRestResponse response = client.Execute(request);
            json_response = response.Content;
            Console.WriteLine("request: " + json_response);

            //clean auth_token
            auth_token = "";
            return json_response;
        }
        private string getShoperAuthToken()
        {
            //get auth_token
            var login = new RestRequest("/webapi/rest/auth", Method.POST);
            login.AddParameter("client_id", "testapi");
            login.AddParameter("client_secret", "aTTjRBD1");
            IRestResponse login_response = client.Execute(login);
            dynamic json = JsonConvert.DeserializeObject(login_response.Content);
            Console.WriteLine("Auth_token: " + json.access_token);
            return json.access_token;
        }
        private string getShoperCountAuction()
        {
            var count = JsonConvert.DeserializeObject<RootObject>(getShoperListAuctions(getShoperAuthToken()));
            return count.count;
        }


    }
}
